# HElib Examples

1. To run the these examples, you first need to install dependencies using the provided `bootstrap.sh` script.
```bash
cd ../../
bash bootstrap.sh
```
Note that make sure, you have the right C++ compiler installed or you can install it using the following commands on Ubuntu 18.04.
```bash
sudo apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y software-properties-common \
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EA8CACC073C3DB2A \
	add-apt-repository ppa:jonathonf/gcc-9.0 -y \
	apt-get update \
	apt-get install -y pkg-config libz-dev git build-essential m4 gcc-9 g++-9 \
	apt-get autoremove -y \
	apt-get clean
```

2. Come back to the this example project sources and run
```bash
cmake .
make
```

3. We will place executable in `../../deps/bin`. Therefore, you can run the example program as
```bash
../../deps/bin/HElibExamples
```